package fly.speedmeter.grub;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.melnykov.fab.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

import static fly.speedmeter.grub.SpeechToTextActivity.RequestPermissionCode;

public class WhiteListActivity extends AppCompatActivity {
    private static final String TAG = WhiteListActivity.class.getSimpleName();
    private final int PICK_CONTACT1 = 1;
    // private final int PICK_CONTACT2 =2;
    //private final int PICK_CONTACT3 =3;
    //private Uri uriContact;
    //private String contactID;
    //public static final int RequestPermissionCode = 1;
    private ArrayList<String> numbers, names;
    public TextView txtView1;
    public TextView txtView2;
    public TextView txtView3;
    public TextView txtView4;

    public TextView txtView5;
    public TextView txtView6;
    FloatingActionButton floatingActionButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_white_list);
        floatingActionButton = findViewById(R.id.whitelistbutton);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callContacts1();
            }
        });
        numbers = new ArrayList<>();
        names = new ArrayList<>();
        txtView2 = (TextView) findViewById(R.id.txtView2);
        EnableRuntimePermission();
    }

    public void callContacts1() {
        Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
        startActivityForResult(intent, PICK_CONTACT1);
    }


    public static void navigate(Activity startingActivity) {
        Intent i = new Intent(startingActivity, WhiteListActivity.class);
        startingActivity.startActivity(i);
    }


    @Override
    protected void onResume() {
        super.onResume();
        readData();
        ListView contacts = (ListView) findViewById(R.id.contacts_list);
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(WhiteListActivity.this, android.R.layout.simple_list_item_1, names) {
            @NonNull
            @Override
            public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View view = super.getView(position, convertView, parent);

                // Initialize a TextView for ListView each Item
                TextView tv = (TextView) view.findViewById(android.R.id.text1);

                // Set the text color of TextView (ListView Item)
                tv.setTextColor(Color.BLACK);

                // Generate ListView Item using TextView
                return view;

            }
        };
        contacts.setAdapter(adapter);
        contacts.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                names.remove(i);

                savewhitelist();
                adapter.notifyDataSetChanged();
                return true;
            }
        });
    }

    protected void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);
        String contactNumber = null;
        if (resultCode == RESULT_OK) {
            if (names.size() >= 3) {
                Toast.makeText(this, "Contact list is full..Long press to remove a contact and then add new..", Toast.LENGTH_SHORT).show();
                return;
            }

            Uri contactData = data.getData();
            String number = "";
            String n1 = "";
            Cursor cursor = getContentResolver().query(contactData, null, null, null, null);
            cursor.moveToFirst();
            String hasPhone = cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.Contacts.HAS_PHONE_NUMBER));
            String name = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
            String contactId = cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.Contacts._ID));
            Log.d(TAG, "onActivityResult: hasPhone: "+hasPhone +"name: "+name+" "+contactId);
            if (hasPhone.equals("1")) {
                Cursor phones = getContentResolver().query
                        (ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                                ContactsContract.CommonDataKinds.Phone.CONTACT_ID
                                        + " = " + contactId, null, null);
                while (phones.moveToNext()) {
                    number = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)).replaceAll("[-() ]", "");
                    n1 = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                }
                phones.close();
            }
            cursor.close();

            Log.d(TAG, "onActivityResult: number: "+number +" n1"+ n1);

            if (names.contains(n1)) {
                Toast.makeText(this, n1 + " already exist.", Toast.LENGTH_LONG).show();
                return;
            }


            //numbers.add(n1);
            names.add(n1);
            numbers.add(number);
            savewhitelist();
            Toast.makeText(this, name + number + " is added to whitelist.", Toast.LENGTH_LONG).show();
        }

        /*if (reqCode == PICK_CONTACT1) {
            if (resultCode == ActionBarActivity.RESULT_OK) {
                Uri contactData = data.getData();
                String number = "";
                Cursor cursor = getContentResolver().query(contactData, null, null, null, null);
                cursor.moveToFirst();
                String hasPhone = cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.Contacts.HAS_PHONE_NUMBER));
                String contactId = cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.Contacts._ID));
                if (hasPhone.equals("1")) {
                    Cursor phones = getContentResolver().query
                            (ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID
                                            + " = " + contactId, null, null);
                    while (phones.moveToNext()) {
                        number = phones.getString(phones.getColumnIndex
                                (ContactsContract.CommonDataKinds.Phone.NUMBER)).replaceAll("[-() ]", "");
                        Toast.makeText(this, number, Toast.LENGTH_LONG).show();
                        txtView1.setText(number);
                    }

                    phones.close();
                    //Do something with number
                } else {
                    Toast.makeText(getApplicationContext(), "This contact has no phone number", Toast.LENGTH_LONG).show();
                }
                cursor.close();
            }
        } else if (reqCode == PICK_CONTACT2) {
            if (resultCode == ActionBarActivity.RESULT_OK) {
                Uri contactData = data.getData();
                String number2 = "";
                Cursor cursor = getContentResolver().query(contactData, null, null, null, null);
                cursor.moveToFirst();
                String hasPhone = cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.Contacts.HAS_PHONE_NUMBER));
                String contactId = cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.Contacts._ID));
                if (hasPhone.equals("1")) {
                    Cursor phones = getContentResolver().query
                            (ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID
                                            + " = " + contactId, null, null);
                    while (phones.moveToNext()) {
                        number2 = phones.getString(phones.getColumnIndex
                                (ContactsContract.CommonDataKinds.Phone.NUMBER)).replaceAll("[-() ]", "");
                        Toast.makeText(this, number2, Toast.LENGTH_LONG).show();
                        txtView2.setText(number2);
                    }

                    phones.close();
                    //Do something with number
                } else {
                    Toast.makeText(getApplicationContext(), "This contact has no phone number", Toast.LENGTH_LONG).show();
                }
                cursor.close();
            }
        } else if (reqCode == PICK_CONTACT3) {
            if (resultCode == ActionBarActivity.RESULT_OK) {
                Uri contactData = data.getData();
                String number3 = "";
                Cursor cursor = getContentResolver().query(contactData, null, null, null, null);
                cursor.moveToFirst();
                String hasPhone = cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.Contacts.HAS_PHONE_NUMBER));
                String contactId = cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.Contacts._ID));
                if (hasPhone.equals("1")) {
                    Cursor phones = getContentResolver().query
                            (ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID
                                            + " = " + contactId, null, null);
                    while (phones.moveToNext()) {
                        number3 = phones.getString(phones.getColumnIndex
                                (ContactsContract.CommonDataKinds.Phone.NUMBER)).replaceAll("[-() ]", "");
                        Toast.makeText(this, number3, Toast.LENGTH_LONG).show();
                        txtView3.setText(number3);
                    }

                    phones.close();
                    //Do something with number
                } else {
                    Toast.makeText(getApplicationContext(), "This contact has no phone number", Toast.LENGTH_LONG).show();
                }
                cursor.close();
            }
        }*/
    }

    public void savewhitelist() {
        SharedPreferences sharedPref = getSharedPreferences("WhiteListInfo", Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPref.edit();
        if (names.size() >= 3) {
            editor.putString("Contacts1", numbers.get(0));
            editor.putString("Contacts2", numbers.get(1));
            editor.putString("Contacts3", numbers.get(2));
        } else if (names.size() == 2) {
            editor.putString("Contacts1", numbers.get(0));
            editor.putString("Contacts2", numbers.get(1));
            editor.putString("Contacts3", "");
        } else if (names.size() == 1) {
            editor.putString("Contacts1", numbers.get(0));
            editor.putString("Contacts2", "");
            editor.putString("Contacts3", "");
        } else if (names.size() == 0) {
            editor.putString("Contacts1", "");
            editor.putString("Contacts2", "");
            editor.putString("Contacts3", "");
        }
        editor.apply();
    }

   /* public void saveInfo(View v)
    {
        SharedPreferences sharedPref = getSharedPreferences("WhiteListInfo", Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("Contact1",txtView1.getText().toString());
        editor.putString("Contact2",txtView2.getText().toString());
        editor.putString("Contact3",txtView3.getText().toString());
        editor.commit();
        Toast.makeText(this,"Saved", Toast.LENGTH_LONG).show();

    }*/

    private void readData() {
        numbers.clear();
        SharedPreferences sharedPref = getSharedPreferences("WhiteListInfo", Context.MODE_PRIVATE);
        String tmp = sharedPref.getString("Contacts1", " ");
        if (tmp.length() > 9)
            numbers.add(tmp);
        tmp = sharedPref.getString("Contacts2", " ");
        if (tmp.length() > 9)
            numbers.add(tmp);
        tmp = sharedPref.getString("Contacts3", " ");
        if (tmp.length() > 9)
            numbers.add(tmp);
    }

    public void displayData() {
        SharedPreferences sharedPref = getSharedPreferences("WhiteListInfo", Context.MODE_PRIVATE);
        String c1 = sharedPref.getString("Contact1", " ");
        String c2 = sharedPref.getString("Contact2", " ");
        String c3 = sharedPref.getString("Contact3", " ");
        Toast.makeText(this, c1, Toast.LENGTH_LONG).show();
        txtView4.setText(c1);
        txtView5.setText(c2);
        txtView6.setText(c3);

    }


    public void EnableRuntimePermission() {

        if (ActivityCompat.shouldShowRequestPermissionRationale(
                WhiteListActivity.this,
                Manifest.permission.READ_CONTACTS)) {

        } else {

            ActivityCompat.requestPermissions(WhiteListActivity.this, new String[]{
                    Manifest.permission.READ_CONTACTS}, RequestPermissionCode);

        }
    }

    @Override
    public void onRequestPermissionsResult(int RC, String per[], int[] PResult) {

        switch (RC) {

            case RequestPermissionCode:

                if (PResult.length > 0 && PResult[0] == PackageManager.PERMISSION_GRANTED) {

                    Log.d(TAG, "onRequestPermissionsResult: ");

                } else {

                    Toast.makeText(WhiteListActivity.this, "Permission Canceled, Now your application cannot access CONTACTS.", Toast.LENGTH_LONG).show();

                }
                break;
        }
    }
}
