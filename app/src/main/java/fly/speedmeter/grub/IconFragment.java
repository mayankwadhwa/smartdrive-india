package fly.speedmeter.grub;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class IconFragment extends Fragment {

    private ImageView mIconImageView;
    private TextView mTextView;
    public OnClickCallback callback;

    public interface OnClickCallback {
        void onClick();
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_icon,container,false);
        mIconImageView = v.findViewById(R.id.iv_icon);
        mTextView = v.findViewById(R.id.tv_title);
        int res = getArguments().getInt("icon_res");
        mIconImageView.setImageResource(res);
        mTextView.setText(getArguments().getInt("title_res"));
        mIconImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callback.onClick();
            }
        });
        return v;
    }

    public static IconFragment newInstance(int iconRes, int titleRes) {
        Bundle args = new Bundle();
        IconFragment fragment = new IconFragment();
        args.putInt("icon_res", iconRes);
        args.putInt("title_res", titleRes);
        fragment.setArguments(args);
        return fragment;
    }
}
