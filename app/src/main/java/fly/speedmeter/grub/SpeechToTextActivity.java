package fly.speedmeter.grub;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.speech.RecognizerIntent;
import android.support.v7.app.AppCompatActivity;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Locale;

public class SpeechToTextActivity extends AppCompatActivity {
    public final int REQ_CODE_SPEECH_INPUT = 100;
    public EditText textView;
    private  final int PICK_CONTACT =1;
    private Uri uriContact;
    private String contactID;
    public  static final int RequestPermissionCode  = 1 ;
    String phonenumber ;
    public EditText txtView2;
    ImageView imgbtn,imgbtn2,imgbtn3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.speech_layout);
        textView =(EditText)findViewById(R.id.textView);
        txtView2 =(EditText)findViewById(R.id.txtView2);
        imgbtn = (ImageView)findViewById(R.id.micbtn);
        imgbtn2 =(ImageView)findViewById(R.id.contact);
        imgbtn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callContacts();
            }
        });
        imgbtn3 =(ImageView)findViewById(R.id.sendbtn);
        imgbtn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendmsg();
            }
        });
       // EnableRuntimePermission();
    }


    public static void navigate(Activity startingActivity) {
        Intent i = new Intent(startingActivity, SpeechToTextActivity.class);
        startingActivity.startActivity(i);
    }


    public void sendmsg()
    {
        String phoneNo=txtView2.getText().toString();
        String message=textView.getText().toString();
       // Toast.makeText(this,"Success1", Toast.LENGTH_LONG).show();
        if(phoneNo.length()>0 && message.length()>0)
        {
            //Toast.makeText(this,"Success2", Toast.LENGTH_LONG).show();
             sendSMS(phoneNo,message);}
        else
            Toast.makeText(getBaseContext(),"Please enter both Phone number and message", Toast.LENGTH_SHORT).show();
    }


    public void sendSMS(String phoneNumber, String message) {
        //Toast.makeText(this,"Success2",Toast.LENGTH_LONG).show();
        PendingIntent pi= PendingIntent.getActivity(getApplicationContext(),0,new Intent(getApplicationContext(),SpeechToTextActivity.class),0);
        SmsManager sms = SmsManager.getDefault();
        sms.sendTextMessage(phoneNumber, null, message, pi, null);
        Toast.makeText(getApplicationContext(), "Message sent successfully!!!", Toast.LENGTH_SHORT).show();
    }

    public void get(View v)
    {
       // Toast.makeText(getApplicationContext(),"Working", Toast.LENGTH_LONG).show();
        askSpeechInput();
    }

    public void askSpeechInput() {
        ///Toast.makeText(getApplicationContext(),"Working2", Toast.LENGTH_LONG).show();
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
                "Hi speak something");
        if(intent.resolveActivity(getPackageManager()) != null)
        {
            startActivityForResult(intent, REQ_CODE_SPEECH_INPUT);
        }
        else
        {
            Toast.makeText(this, "Your device doesnt support this device", Toast.LENGTH_LONG).show();
        }

    }

    // Receiving SpeechToTextActivity input

   /* @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Toast.makeText(getApplicationContext(),"Working4",Toast.LENGTH_LONG).show();
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQ_CODE_SPEECH_INPUT: {
                if (resultCode == RESULT_OK && null != data) {

                    ArrayList<String> result = data
                            .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    textView.setText(result.get(0));
                }
                break;
            }

        }
    } */

   public void callContacts()
    {
        //Toast.makeText(this,"SUCCESS",Toast.LENGTH_LONG).show();
       Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
        startActivityForResult(intent,PICK_CONTACT);
    }
    protected void onActivityResult(int reqCode, int resultCode, Intent data)
    {
        super.onActivityResult(reqCode,resultCode, data);
        String contactNumber = null;
        if(reqCode == PICK_CONTACT)
        {
            if(resultCode == AppCompatActivity.RESULT_OK)
            {
                Uri contactData = data.getData();
                String number = "";
                Cursor cursor = getContentResolver().query(contactData, null, null, null, null);
                cursor.moveToFirst();
                String hasPhone = cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.Contacts.HAS_PHONE_NUMBER));
                String contactId = cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.Contacts._ID));
                if (hasPhone.equals("1")) {
                    Cursor phones = getContentResolver().query
                            (ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID
                                            + " = " + contactId, null, null);
                    while (phones.moveToNext()) {
                        number = phones.getString(phones.getColumnIndex
                                (ContactsContract.CommonDataKinds.Phone.NUMBER)).replaceAll("[-() ]", "");
                      //  Toast.makeText(this,number, Toast.LENGTH_LONG).show();
                        txtView2.setText(number);
                    }

                    phones.close();
                    //Do something with number
                } else {
                    Toast.makeText(getApplicationContext(), "This contact has no phone number", Toast.LENGTH_LONG).show();
                }
                cursor.close();
            }
        }

        else if(reqCode == REQ_CODE_SPEECH_INPUT)
        {
            if (resultCode == RESULT_OK && null != data) {

                ArrayList<String> result = data
                        .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                textView.setText(result.get(0));
            }
            //break ;
        }
    }




   /* public void EnableRuntimePermission(){

        if (ActionBarActivity.shouldShowRequestPermissionRationale(
                SpeechToTextActivity.this,
                Manifest.permission.READ_CONTACTS))
        {

            Toast.makeText(SpeechToTextActivity.this,"CONTACTS permission allows us to Access CONTACTS app", Toast.LENGTH_LONG).show();
        } else {

            ActionBarActivity.requestPermissions(SpeechToTextActivity.this,new String[]{
                    Manifest.permission.READ_CONTACTS}, RequestPermissionCode);

        }


    @Override
    public void onRequestPermissionsResult(int RC, String per[], int[] PResult) {

        switch (RC) {

            case RequestPermissionCode:

                if (PResult.length > 0 && PResult[0] == PackageManager.PERMISSION_GRANTED) {

                    Toast.makeText(SpeechToTextActivity.this,"Permission Granted, Now your application can access CONTACTS.", Toast.LENGTH_LONG).show();

                } else {

                    Toast.makeText(SpeechToTextActivity.this,"Permission Canceled, Now your application cannot access CONTACTS.", Toast.LENGTH_LONG).show();

                }
                break;
        }
    }*/

}
