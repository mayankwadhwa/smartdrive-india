package fly.speedmeter.grub;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;


import github.chenupt.springindicator.SpringIndicator;


public class HomeActivity extends AppCompatActivity {

    private ViewPager mViewPager;
    private SpringIndicator springIndicator;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        mViewPager = findViewById(R.id.mainPager);
        springIndicator = findViewById(R.id.indicator);
        mViewPager.setAdapter(new FragmentStatePagerAdapter(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(final int position) {

                IconFragment.OnClickCallback clickCallback = new IconFragment.OnClickCallback() {
                    @Override
                    public void onClick() {
                        switch (position) {
                            case 0:
                                SpeedActivity.navigate(HomeActivity.this);
                                break;
                            case 1:
                                WhiteListActivity.navigate(HomeActivity.this);
                                break;
                            case 2:
                                SpeechToTextActivity.navigate(HomeActivity.this);
                                break;
                            case 3:
                                final Intent intent = new Intent(Intent.ACTION_MAIN, null);
                                intent.addCategory(Intent.CATEGORY_LAUNCHER);
                                ComponentName cn = new ComponentName("com.android.settings",
                                        "com.android.settings.bluetooth.BluetoothSettings");
                                intent.setComponent(cn);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                                break;
                            case 4:
                                Uri gmmIntentUri = Uri.parse("geo:0,0");
                                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                                mapIntent.setPackage("com.google.android.apps.maps");
                                startActivity(mapIntent);
                                break;
                            case 5:
                                Intent intent1 = new Intent(MediaStore.INTENT_ACTION_MUSIC_PLAYER);
                                startActivity(intent1);
                                break;


                        }
                    }
                };

                switch (position) {
                    case 0:
                        IconFragment iconFragment = IconFragment.newInstance(R.drawable.ic_speedometer, R.string.speedTracker);
                        iconFragment.callback = clickCallback;
                        return iconFragment;
                    case 1:
                        IconFragment iconFragment1 = IconFragment.newInstance(R.drawable.ic_whitelist, R.string.whiteList);
                        iconFragment1.callback = clickCallback;
                        return iconFragment1;

                    case 2:
                        IconFragment iconFragment2 = IconFragment.newInstance(R.drawable.ic_speech, R.string.smart_respo);
                        iconFragment2.callback = clickCallback;
                        return iconFragment2;

                    case 3:
                        IconFragment iconFragment3 = IconFragment.newInstance(R.drawable.ic_bluetooth, R.string.connect_to_bluetooth);
                        iconFragment3.callback = clickCallback;
                        return iconFragment3;

                    case 4:
                        IconFragment iconFragment4 = IconFragment.newInstance(R.drawable.ic_navigation, R.string.open_maps);
                        iconFragment4.callback = clickCallback;
                        return iconFragment4;

                    case 5:
                        IconFragment iconFragment5 = IconFragment.newInstance(R.drawable.ic_library_music, R.string.open_music);
                        iconFragment5.callback = clickCallback;
                        return iconFragment5;


                }
                return null;
            }

            @Override
            public int getCount() {
                return 6;
            }

        });

        springIndicator.setViewPager(mViewPager);

    }

    public static void navigate(Activity startingActivity) {
        Intent i = new Intent(startingActivity, HomeActivity.class);
        startingActivity.startActivity(i);
    }


}


