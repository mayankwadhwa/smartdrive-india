package fly.speedmeter.grub;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

import com.android.internal.telephony.ITelephony;

import java.lang.reflect.Method;
import java.util.ArrayList;

import static android.content.Intent.getIntent;

public class PhoneCallReceiver extends android.content.BroadcastReceiver {
    static boolean ring = false;
    static boolean callReceived = false;
    //  public TextView phone_text_view;

    private static final String TAG = "Phone call";
    private ITelephony telephonyService;

    static double cur_speed = 0;

    static ArrayList<String> missed_numbers = new ArrayList<>();

    /*Intent in = new Intent(getIntent());
    private double sp = in.getExtras().getString("curSpeed");*/

    static void update_speed(double current_speed) {
        cur_speed = current_speed;
    }


    @Override
    public void onReceive(Context context, Intent intent) {

        Log.d("Broadcast::speedmeter", "Broadcast received");
        //double s = Data.getCurSpeed();

        if (cur_speed >= 20) {
            String num = intent.getStringExtra(TelephonyManager.EXTRA_INCOMING_NUMBER);
            int flag = 0;
            ArrayList<String> whitenumbers = getcontacts(context);
            for (String whnum : whitenumbers) {
                if (num.contains(whnum)) {
                    flag = 1;
                }
            }
            if (flag == 0) {


                Log.v(TAG, "Receving....");
                TelephonyManager telephony = (TelephonyManager)
                        context.getSystemService(Context.TELEPHONY_SERVICE);
                try {
                    Class c = Class.forName(telephony.getClass().getName());
                    Method m = c.getDeclaredMethod("getITelephony");
                    m.setAccessible(true);
                    telephonyService = (ITelephony) m.invoke(telephony);
                    //telephonyService.silenceRinger();
                    telephonyService.endCall();
                    update_notification(context, new String(num));

                    if (num != null) {
                        // phone_text_view.setText("It was A MISSED CALL from : "+num);
                        Toast.makeText(context, "It was A MISSED CALL from : " + num, Toast.LENGTH_LONG).show();
                    }

                    // PendingIntent pi = PendingIntent.getActivity(this,0,new Intent(this,PhoneCallReceiver.class),0);
                    SmsManager sms = SmsManager.getDefault();
                    sms.sendTextMessage(num, null, "The person to whom you are trying to call is currently driving. Please call again later.", null, null);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public ArrayList<String> getcontacts(Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences("WhiteListInfo", Context.MODE_PRIVATE);
        ArrayList<String> whitenumbers = new ArrayList<>();
        String tmp = sharedPref.getString("Contacts1", " ");
        if (tmp.length() > 9)
            whitenumbers.add(tmp);
        tmp = sharedPref.getString("Contacts2", " ");
        if (tmp.length() > 9)
            whitenumbers.add(tmp);
        tmp = sharedPref.getString("Contacts3", " ");
        if (tmp.length() > 9)
            whitenumbers.add(tmp);
        return whitenumbers;
    }

    private void update_notification(Context context, String missed_num) {
        String message = "You got a missed call from " + missed_num;
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("Missed Call")
                .setContentText(message)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);

        int notificationId = 123;
// notificationId is a unique int for each notification that you must define
        notificationManager.notify(notificationId, mBuilder.build());
    }
}

